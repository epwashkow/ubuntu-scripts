#!/bin/bash

# https://github.com/nvm-sh/nvm#installing-and-updating
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash

echo "Close and open a new terminal, then run"
echo "nvm install --lts"